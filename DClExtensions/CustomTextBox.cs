﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace DClExtensions
{
    public class CustomTextBox : TextBox
    {
        object _oldValue = null;

        private void _toggleFocus(FocusNavigationDirection direction = FocusNavigationDirection.Next)
        {
            this.MoveFocus(new TraversalRequest(direction));
        }

        protected override void OnGotFocus(System.Windows.RoutedEventArgs e)
        {
            base.OnGotFocus(e);

            _oldValue = Text;
            SelectAll();
        }

        protected override void OnKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            base.OnKeyDown(e);

            switch (e.Key)
            {
                case Key.Escape:
                    Text = _oldValue.ToString();
                    _toggleFocus();
                    break;

                case Key.Enter:
                    _toggleFocus(FocusNavigationDirection.Next);
                    break;
            }
        }
    }
}
