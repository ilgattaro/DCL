﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DrawControlLibrary.Tools;
using DrawControlLibrary;

namespace DClExtensions
{
    /// <summary>
    /// Contiene gli strumenti di editing
    /// </summary>
    public partial class DCExtPaletteUC : UserControl
    {
        /// <summary>
        /// Ottiene o imposta il canvas
        /// </summary>
        public DCCanvas Canvas { get; set; }

        /// <summary>
        /// Costruttore
        /// </summary>
        public DCExtPaletteUC()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Aggiunge un tool dinamicamente alla palette
        /// </summary>
        /// <param name="tool"></param>
        /// <param name="img"></param>
        public void AddTool(DCAbstractTool tool, ImageSource img)
        {
            Button btn = new Button();
            btn.Content = new Image() { Source = img };
            btn.Tag = tool;
            btn.Click += btn_Click;
            palette.Children.Add(btn);
        }


        private void lineTool_Click(object sender, RoutedEventArgs e)
        {
            if (Canvas == null)
                return;
            Canvas.SetTool(new DCLineTool());
        }

        private void polygonTool_Click(object sender, RoutedEventArgs e)
        {
            if (Canvas == null)
                return;
            Canvas.SetTool(new DCPolygonTool());
        }

        private void textTool_Click(object sender, RoutedEventArgs e)
        {
            if (Canvas == null)
                return;
            Canvas.SetTool(new DCTextTool());
        }

        private void pictureTool_Click(object sender, RoutedEventArgs e)
        {
            if (Canvas == null)
                return;
            Canvas.SetTool(new DCImageTool());
        }

        private void ellipseTool_Click(object sender, RoutedEventArgs e)
        {
            if (Canvas == null)
                return;
            Canvas.SetTool(new DCEllipseTool());
        }

        private void selectTool_Click(object sender, RoutedEventArgs e)
        {
            if (Canvas == null)
                return;
            Canvas.SetTool(null);
        }

        private void rectTool_Click(object sender, RoutedEventArgs e)
        {
            if (Canvas == null)
                return;
            Canvas.SetTool(new DCRectangleTool());
        }

        private void cloudTool_Click(object sender, RoutedEventArgs e)
        {
            if (Canvas == null)
                return;
            Canvas.SetTool(new DCCloudPolygonTool());
        }

        private void arrowTool_Click(object sender, RoutedEventArgs e)
        {
            if (Canvas == null)
                return;
            Canvas.SetTool(new DCArrowTool());
        }

        void btn_Click(object sender, RoutedEventArgs e)
        {
            Canvas.SetTool((DCAbstractTool)((sender as Button).Tag));
        }
    }
}
