﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DrawControlLibrary.Entities
{
    /// <summary>
    /// Entità Immagine
    /// </summary>
    public class DCImage : DCHost
    {
        // numero progressivo per le immagini cropped
        private static int _progressiveExport=0;

        private Image _image;


        #region Proprietà

        /// <summary>
        /// Ottiene o imposta la sorgente dell'immagine
        /// </summary>
        public ImageSource Src
        {
            get { return (ImageSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        public static readonly DependencyProperty SourceProperty =
           DependencyProperty.Register("Src", typeof(ImageSource), typeof(DCImage), new UIPropertyMetadata(null));

        /// <summary>
        /// Ottiene la larghezza originale
        /// </summary>
        public double OriginalImageWidth
        {
            get{ return Src.Width; }
        }

        /// <summary>
        /// Ottiene l'atlezza originale
        /// </summary>
        public double OriginalImageHeight
        {
            get { return Src.Height; }
        }

        #endregion


        /// <summary>
        /// Costruttore
        /// </summary>
        public DCImage() : base()
        {
            // istanzia immagine e setta il content
            _image = new Image();

            // crea bindings

            Binding bindingSource = new Binding("Src");
            bindingSource.Source = this;

            _image.SetBinding(Image.SourceProperty, bindingSource);
            _image.Stretch = Stretch.Fill;

            // setta come content
            Content = _image;
        }


        // override serializzazione
        public override string _getSerialization(object param = null)
        {
            string imageFileName = string.Empty;

            // elaborazione del nome del file

                // se ho una cropped bitmap o rtb deve salvare l'immagine in un file e includerla nel documento
                if (Src is CroppedBitmap || Src is RenderTargetBitmap)
                {
                    // il parametro passato sarà il file da salvare
                    string path = Path.GetFullPath(param.ToString());
                    path = Path.GetDirectoryName(path);
                    string file = Path.GetFileNameWithoutExtension(param.ToString());

                    // costruisco path
                    imageFileName = path + "\\" + file + "_" + (++_progressiveExport) + ".png";

                    // salva l'immagine(altrimenti non serializza)
                    if (!DCUtils.SaveImage(Src as BitmapSource, imageFileName))
                        return string.Empty;
                }
                else
                {
                    imageFileName = Src.ToString();
                }

            return string.Format
                (
                "<DCImage background='{0}' borderBrush='{1}' borderThickness='{2}' position='{3}' rotation='{4}' scale='{5}' data='{6}' id='{7}' width='{8}' height='{9}' src='{10}' effect='{11}' />",
                DCSupport.GetBrushStr(Background),
                DCSupport.GetBrushStr(BorderBrush),
                BorderThickness,
                Position.ToString(DCDefinitions.NumberFormat),
                Rotation.ToString(DCDefinitions.NumberFormat),
                Scale.ToString(DCDefinitions.NumberFormat),
                DCSupport.GetDataStr(Data),
                Id,
                ActualWidth.ToString(DCDefinitions.NumberFormat),
                ActualHeight.ToString(DCDefinitions.NumberFormat),
                imageFileName,
                DCSupport.GetEffectStr(Effect)
                );
        }
    }
}
