﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DrawControlLibrary.Managers;

namespace DrawControlLibrary.Entities
{
    /// <summary>
    /// Entità Host-User Control(Aggregatore di elementi WPF)
    /// </summary>
    public class DCHost : UserControl, IDCEntity, INotifyPropertyChanged
    {
        #region Proprietà Comuni

        /// <summary>
        /// Ottiene o imposta l'ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Ottiene o imposta il manager dei punti di controllo
        /// </summary>
        public DCAbstractControlPointMngr ControlPointsManager { get; set; }

        /// <summary>
        /// Ottiene o imposta se i punti di controllo sono editabili
        /// </summary>
        public bool Editable { get; set; }

        /// <summary>
        /// Ottiene o imposta se è possibile cancellare l'entità
        /// </summary>
        public bool Deletable { get; set; }

        /// <summary>
        /// Ottiene o imposta se l'entità può essere selezionata
        /// </summary>
        public bool Selectable { get; set; }

        /// <summary>
        /// Ottiene o imposta il flag se l'entità è di sistema
        /// </summary>
        public bool IsSystemEntity { get; set; }

        /// <summary>
        /// Ottiene o imposta i dati associati a questa entità
        /// </summary>
        public Dictionary<string, object> Data { get; set; }


        /// <summary>
        /// Ottiene o imposta la posizione
        /// </summary>
        [DisplayName("Coordinate")]
        public Point Position
        {
            get { return this.GetPosition().Value; }
            set
            {
                DCCore.OnPositionChanged(this, value);
                FirePropertyChanged("Position");
            }
        }


        /// <summary>
        /// Ottiene o imposta la rotazione
        /// </summary>
        [DisplayName("Rotazione")]
        public double Rotation
        {
            get { return _rotateTransform.Angle; }
            set
            {
                _onRotate(value);
                FirePropertyChanged("Rotation");
            }
        }


        /// <summary>
        /// Ottiene o imposta la scala
        /// </summary>
        [DisplayName("Scala")]
        public Point Scale
        {
            get { return new Point(_scaleTransform.ScaleX, _scaleTransform.ScaleY); }
            set
            {
                if (value != null)
                {
                    _onScale(value.X, value.Y);
                    FirePropertyChanged("Scale");
                }
            }
        }


        /// <summary>
        /// Ottine o imposta la selezione
        /// </summary>
        [DisplayName("Selezionato")]
        public bool Selected
        {
            get { return (bool)GetValue(SelectedProperty); }
            set
            {
                if (Selectable == true)
                    SetValue(SelectedProperty, value);
            }
        }
        public static readonly DependencyProperty SelectedProperty = DependencyProperty.Register("Selected", typeof(bool), typeof(DCHost), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender, new PropertyChangedCallback(DCCore.OnSelect)));


        /// <summary>
        /// Ottiene o imposta il tipo di movimento consentito in un evento di spostamento
        /// </summary>
        public DCMoveableType MoveableType
        {
            get { return (DCMoveableType)GetValue(MoveableTypeProperty); }
            set { SetValue(MoveableTypeProperty, value); }
        }
        public static readonly DependencyProperty MoveableTypeProperty = DependencyProperty.Register("MoveableType", typeof(DCMoveableType), typeof(DCHost), new UIPropertyMetadata(DCMoveableType.MoveableFree));


        // eventi registrabili


        /// <summary>
        /// Questo evento viene chiamato quando si verifica un drag di questa entità
        /// </summary>
        public event EventHandler MoveEvent;

        /// <summary>
        /// Questo evento viene chiamato quando si modifica un punto di controllo di questa entità
        /// </summary>
        public event EventHandler<DCEditEventArgs> EditEvent;

        /// <summary>
        /// Questo evento viene chiamato quando si seleziona l'entità
        /// </summary>
        public event EventHandler<DCSelectionEventArgs> SelectedEvent;

        /// <summary>
        /// Questo evento viene chiamato quando si deseleziona l'entità
        /// </summary>
        public event EventHandler<DCSelectionEventArgs> UnSelectedEvent;

        /// <summary>
        /// Questo evento viene chiamato quando viene disegnata l'entità
        /// </summary>
        public event RenederDelegate RenderEvent;

        #endregion


        /// <summary>
        /// Costruttore
        /// </summary>
        public DCHost() : base()
        {
            // inizializza l'entità
            DCCore.OnCreate(this);

            // questo deve supportare il focus da tastiera
            Focusable = true;

            // inizializza trasformazioni
            _transformGroup = new TransformGroup();
            _scaleTransform = new ScaleTransform(1, 1, 0, 0);
            _rotateTransform = new RotateTransform(0, 0, 0);

            _transformGroup.Children.Add(_scaleTransform);
            _transformGroup.Children.Add(_rotateTransform);

            // associa l'evento di inizializzazione
            this.Initialized += new EventHandler(DCHost_Initialized);
        }

        
        #region Implementazioni
        public virtual string _getSerialization(object param = null)
        {
            string head = string.Format
                (
                "<DCHost background='{0}' borderBrush='{1}' borderThickness='{2}' position='{3}' rotation='{4}' scale='{5}' data='{6}' id='{7}' width='{8}' height='{9}' foreground='{10}' fontSize='{11}' fontFamily='{12}' horizontalContentAlignment='{13}' verticalContentAlignment='{14}' effect='{15}'>",
                DCSupport.GetBrushStr(Background), 
                DCSupport.GetBrushStr(BorderBrush), 
                BorderThickness, 
                Position.ToString(DCDefinitions.NumberFormat), 
                Rotation.ToString(DCDefinitions.NumberFormat), 
                Scale.ToString(DCDefinitions.NumberFormat), 
                DCSupport.GetDataStr(Data), 
                Id, 
                ActualWidth.ToString(DCDefinitions.NumberFormat), 
                ActualHeight.ToString(DCDefinitions.NumberFormat),
                DCSupport.GetBrushStr(Foreground),
                FontSize.ToString(DCDefinitions.NumberFormat),
                FontFamily.ToString(),
                (int)HorizontalContentAlignment,
                (int)VerticalContentAlignment,
                DCSupport.GetEffectStr(Effect)
                );

            string body="";
            try
            {
                body=XamlWriter.Save(Content);
            } 
            catch(Exception ex)
            {
                body=ex.Message;
            }

            return head + "<![CDATA[" + body + "]]></DCHost>";
        }

        public System.Drawing.RectangleF GetBounds()
        {
            // calcolo posizione esatta
            double xPos = Position.X;
            double yPos = Position.Y;

            // ritorno bounding box
            return new System.Drawing.RectangleF(
                (float)xPos,
                (float)yPos,
                (float)ActualWidth,
                (float)ActualHeight
                );
        }

        public Point GetCenter()
        {
            // recupera il bound della geometria
            System.Drawing.RectangleF bounds = GetBounds();

            return new Point(bounds.Width / 2, bounds.Height / 2);
        }

        public DCCanvas GetCanvas()
        {
            DCCanvas canvas = Parent as DCCanvas;
            if (canvas == null)
                canvas = VisualTreeHelper.GetParent(this) as DCCanvas;
            if (canvas == null)
                canvas = DCUtils.FindParent<DCCanvas>(this);
            return canvas;
        }




        // handling eventi
        public void _notifyMoveEvent()
        {
            if (MoveEvent != null)
                MoveEvent(this, null);
        }

        public void _notifyEditEvent(int pointIndex)
        {
            DCEditEventArgs e = new DCEditEventArgs(pointIndex);
            if (EditEvent != null)
                EditEvent(this, e);
        }

        public void _notifySelectedEvent(DCSelectionType type)
        {
            if (SelectedEvent != null)
                SelectedEvent(this, new DCSelectionEventArgs(type));
        }

        public void _notifyUnSelectedEvent(DCSelectionType type)
        {
            if (UnSelectedEvent != null)
                UnSelectedEvent(this, new DCSelectionEventArgs(type));
        }

        // i metodi di scale e rotate sono beta
        private void _onRotate(double deg)
        {
            _rotateTransform.Angle = deg;

            UpdateLayout();
            this.UpdateAll();
        }

        private void _onScale(double factorX, double factorY)
        {
            double centerX = 0;
            double centerY = 0;

            //Point center = GetCenter();
            //centerX = center.X;
            //centerY = center.Y;

            _scaleTransform.ScaleX = factorX;
            _scaleTransform.ScaleY = factorY;
            _scaleTransform.CenterX = centerX;
            _scaleTransform.CenterY = centerY;

            UpdateLayout();
            this.UpdateAll();
        }
        #endregion


        #region Interfaccia INotify
        public event PropertyChangedEventHandler PropertyChanged;
        public void FirePropertyChanged(string propertyName)
        {
            try
            {
                Debug.Assert(!String.IsNullOrEmpty(propertyName));
                if (String.IsNullOrEmpty(propertyName))
                    throw new ArgumentNullException("propertyName");

                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
            }
        }
        #endregion


        #region Altro
        private bool _flagOutside;
    
        // trasformazioni
        private RotateTransform _rotateTransform;
        private ScaleTransform _scaleTransform;
        private TransformGroup _transformGroup;

        [DisplayName("Larghezza")]
        public double HostWidth
        {
            get { return ActualWidth; }
            set { Width = value; }
        }

        [DisplayName("Larghezza")]
        public double HostHeight
        {
            get { return ActualHeight; }
            set { Height = value; }
        }

        public RotateTransform RotationTransform
        {
            get { return _rotateTransform; }
        }

        public ScaleTransform ScalingTransform
        {
            get { return _scaleTransform; }
        }


        // metodi di override
        public virtual void OnHideContent() { }
        public virtual void OnShowContent() { }

        /// <summary>
        /// Restituisce vero se l'host sta ricevendo l'input utente
        /// </summary>
        /// <returns></returns>
        public virtual bool HasInteraction()
        {
            // bool condiction = IsKeyboardFocusWithin; // old default;
            bool condiction = IsFocused || IsKeyboardFocusWithin || IsKeyboardFocused;

            return condiction;
        }

        public void SetRotationAngle(double deg) { _rotateTransform.Angle = deg; }

        
        internal void FireChanges()
        {
            FirePropertyChanged("HostWidth");
            FirePropertyChanged("HostHeight");
        }

        // eventi
        void DCHost_Initialized(object sender, EventArgs e)
        {
            // il figlio riceverà la trasformazione
            FrameworkElement content = this.Content as FrameworkElement;
            if (content != null)
            {
                this.RenderTransform = _transformGroup;
                this.RenderTransformOrigin = new Point(0.5, 0.5);
            }
        }

        // handling eventi
        protected override void OnMouseEnter(MouseEventArgs e) { DCCore.OnEnter(this); }
        protected override void OnMouseLeave(MouseEventArgs e) { DCCore.OnLeave(this); }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            #region logica
            // recupera canvas
            DCCanvas canvas = GetCanvas();

            // valida canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // effetto hover
            if ( ( Rotation!=0 && Selected ) || ( IsMouseOver && !IsSystemEntity ) )
                dc.DrawRectangle(Brushes.Transparent, new Pen(Brushes.Blue, 2) , new Rect(0, 0, ActualWidth, ActualHeight) );

            // effetto fuori
            if (Position.X >= canvas.SheetWidth || Position.Y >= canvas.SheetHeight)
            {
                // se non è flaggato
                if (!_flagOutside)
                {
                    // conservo background
                    Data["_oldBackground"] = Background;

                    // ... foreground
                    Data["_oldForeground"] = Foreground;

                    // se il figlio è un controllo conservo background figlio
                    if (this.Content != null && this.Content is Control)
                        Data["_oldBackground2"] = (this.Content as Control).Background;
                    else if (this.Content != null && this.Content is Border)
                        Data["_oldBackoungd2"] = (this.Content as Border).Background;

                    // attivo flag
                    _flagOutside = true;

                    // setto background disabilitato
                    Background = canvas.Background;
                    Foreground = Brushes.DimGray;

                    // nel caso è un immagine fa il grayscale
                    if (this.Content!=null && this.Content is Image)
                    {
                        try
                        {
                            Image image = this.Content as Image;
                            BitmapImage bmpImage = new BitmapImage(new Uri(image.Source.ToString()));
                            image.Source = new FormatConvertedBitmap(bmpImage, PixelFormats.Gray32Float, null, 0);
                        }
                        catch
                        {
                            // non succede nulla se fallirà
                        }
                    }
                    // altrimenti nel caso classico...
                    else if (this.Content!=null && this.Content is Control)
                        (this.Content as Control).Background = canvas.Background;
                    else if (this.Content != null && this.Content is Border)
                        (this.Content as Border).Background = canvas.Background;

                    OnHideContent();
                }

                dc.DrawRectangle(Brushes.Gray, new Pen(Brushes.DimGray, 2), new Rect(0, 0, ActualWidth, ActualHeight));
            }
            else // inverso(si verifica quando ripristina)
            {
                if (_flagOutside) // se ho flaggato
                {
                        OnShowContent();

                        // ripristina background padre
                        Background = Data["_oldBackground"] as Brush;
                       
                        // ... foreground
                        Foreground = Data["_oldForeground"] as Brush;

                        // ripristina background figlio
                        if (this.Content is Control)
                            (this.Content as Control).Background = Data["_oldBackground2"] as Brush;

                        else if (this.Content != null && this.Content is Border)
                            (this.Content as Border).Background = Data["_oldBackground2"] as Brush;

                        else if (this.Content != null && this.Content is Image)
                        {
                            try
                            {
                                Image image = this.Content as Image;
                                FormatConvertedBitmap grayImage = image.Source as FormatConvertedBitmap;
                                if (grayImage != null)
                                {
                                    image.Source = new BitmapImage(new Uri(grayImage.Source.ToString()));
                                    grayImage = null;
                                    GC.Collect();
                                }
                            }
                            catch
                            {
                                // non succede nulla se fallirà
                            }
                        }

                 _flagOutside = false;
                 }
            }

            #endregion

            // fa scattare l'evento di render
            if (RenderEvent != null)
                RenderEvent(dc);
        }
        #endregion


        #region ICloneable Interface
        /// <summary>
        /// Crea un clone
        /// </summary>
        /// <returns></returns>
        public virtual object Clone()
        {
            // crea l'host
            DCHost host = new DCHost();

            // recupera le proprietà
            host.Background = Background != null ? Background.Clone() : null;
            host.BorderBrush = BorderBrush != null ? BorderBrush.Clone() : null;
            host.BorderThickness = new Thickness(BorderThickness.Left, BorderThickness.Top, BorderThickness.Right, BorderThickness.Bottom);
            host.Position = Position.GetClone();
            host.Rotation = Rotation;
            host.Scale = Scale.GetClone();
            host.Id = Id;
            host.Width = ActualWidth;
            host.Height = ActualHeight;
            host.Foreground = Foreground.Clone();
            host.FontSize = FontSize;
            host.FontFamily = new FontFamily(FontFamily.Source);
            
            if (host.Effect != null)
                host.Effect = this.Effect.Clone();

            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            foreach (string key in Data.Keys)
                dictionary.Add(key, Data[key]);

            host.Data = dictionary;

            try
            {
                string body = XamlWriter.Save(Content);
                
                // la condizione non prevede che il contenuto sia uno dei seguenti tipi di bitmap
                bool condiction = !body.Contains("CroppedBitmap") && !body.Contains("RenderTargetBitmap");
                if(condiction)
                {
                    object content = XamlReader.Parse(body);
                    host.Content = content;
                }
                else
                {
                    //TextBlock label = new TextBlock();
                    //    label.TextWrapping = TextWrapping.WrapWithOverflow;
                    //    label.Text="Non è possibile clonare un entità rasterizzata perchè di se è già una copia.";

                    //host.Content = label;

                    return this.GetRaster();
                }
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
                host.Content = null;
            }

            return host;
        }
        #endregion
    }
}
