﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace DrawControlLibrary.Entities
{
    /// <summary>
    /// Entità Testo
    /// </summary>
    public class DCText : DCHost
    {
        private TextBox _textbox;

        #region Proprietà
        /// <summary>
        /// Ottiene o imposta se il testo è di sola lettura
        /// </summary>
        public bool ReadOnly
        {
            get { return (bool)GetValue(ReadOnlyProperty); }
            set { SetValue(ReadOnlyProperty, value); }
        }
        public static readonly DependencyProperty ReadOnlyProperty =
            DependencyProperty.Register("ReadOnly", typeof(bool), typeof(DCText), new UIPropertyMetadata(false));

        /// <summary>
        /// Ottiene o imposta il contenuto del testo
        /// </summary>
        [DisplayName("Testo")]
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set 
            { 
                if(!ReadOnly)
                    SetValue(TextProperty, value);
            }
        }
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(DCText), new UIPropertyMetadata(""));

        #endregion


        /// <summary>
        /// Costruttore
        /// </summary>
        public DCText() : base()
        {
            // istanzia textbox e setta il content
            _textbox = new TextBox();
            _textbox.PreviewMouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(_textbox_PreviewMouseDoubleClick);
            _textbox.MouseEnter += new System.Windows.Input.MouseEventHandler(_textbox_MouseEnter);
            _textbox.BorderBrush = Brushes.Transparent;
            _textbox.BorderThickness = new Thickness(0);
            _textbox.AcceptsReturn = true;

            // crea bindings

            Binding bindingReadOnly = new Binding("ReadOnly");
            bindingReadOnly.Source = this;

            Binding bindingForeground = new Binding("Foreground");
            bindingForeground.Source = this;

            Binding bindingBackground = new Binding("Background");
            bindingBackground.Source = this;

            Binding bindingBorderBrush = new Binding("BorderBrush");
            bindingBorderBrush.Source = this;

            Binding bindingBorderThickness = new Binding("BorderThickness");
            bindingBorderThickness.Source = this;

            Binding bindingFontFamily = new Binding("FontFamily");
            bindingFontFamily.Source = this;

            Binding bindingFontSize = new Binding("FontSize");
            bindingFontSize.Source = this;

            Binding bindingText = new Binding("Text");
            bindingText.Source = this;
            bindingText.Mode = BindingMode.TwoWay;

            _textbox.SetBinding(TextBox.IsReadOnlyProperty, bindingReadOnly);
            _textbox.SetBinding(TextBox.ForegroundProperty, bindingForeground);
            _textbox.SetBinding(TextBox.BackgroundProperty, bindingBackground);
            _textbox.SetBinding(TextBox.FontFamilyProperty, bindingFontFamily);
            _textbox.SetBinding(TextBox.FontSizeProperty, bindingFontSize);
            _textbox.SetBinding(TextBox.BorderBrushProperty, bindingBorderBrush);
            _textbox.SetBinding(TextBox.BorderThicknessProperty, bindingBorderThickness);
            
            _textbox.SetBinding(TextBox.TextProperty, bindingText);

            BorderBrush = Brushes.Black;
            BorderThickness = new Thickness(1.5);

            // setta come content
            Content = _textbox;
        }


        // override serializzazione
        public override string _getSerialization(object param = null)
        {
            return string.Format
                (
                "<DCText background='{0}' borderBrush='{1}' borderThickness='{2}' position='{3}' rotation='{4}' scale='{5}' data='{6}' id='{7}' width='{8}' height='{9}' foreground='{10}' fontSize='{11}' fontFamily='{12}' text='{13}' horizontalContentAlignment='{14}' verticalContentAlignment='{15}' effect='{16}' />",
                DCSupport.GetBrushStr(Background),
                DCSupport.GetBrushStr(BorderBrush),
                BorderThickness,
                Position.ToString(DCDefinitions.NumberFormat),
                Rotation.ToString(DCDefinitions.NumberFormat),
                Scale.ToString(DCDefinitions.NumberFormat),
                DCSupport.GetDataStr(Data),
                Id,
                ActualWidth.ToString(DCDefinitions.NumberFormat),
                ActualHeight.ToString(DCDefinitions.NumberFormat),
                DCSupport.GetBrushStr(Foreground),
                FontSize.ToString(DCDefinitions.NumberFormat),
                FontFamily.ToString(),
                Text,
                (int)HorizontalContentAlignment,
                (int)VerticalContentAlignment,
                DCSupport.GetEffectStr(Effect)
                );
        }


        // eventi
        void _textbox_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            _textbox.Focus();
            Keyboard.Focus(_textbox);
        }

        // gestisce la selezione
        void _textbox_PreviewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //Selected = !Selected;
        }
    }
}
