﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Effects;
using System.Windows;
using System.Diagnostics;
using System.Collections;
using iTextSharp.text.pdf;
using System.IO;
using System.Windows.Controls;

namespace DrawControlLibrary
{
    /// <summary>
    /// Procedure di supporto ad uso esterno
    /// </summary>
    public static class DCHelpers
    {
        /// <summary>
        /// Combine PDF files
        /// </summary>
        /// <param name="fileNames">Array of PDF files(Files must be exists)</param>
        /// <param name="outFile">Output file generated</param>
        /// <returns>true if Success</returns>
        /// <exception cref="Exception">Throw an exception if wrong access to FS</exception>
        public static bool CombineMultiplePDFs(string[] fileNames, string outFile)
        {
            try
            {
                Debug.Assert(fileNames != null && fileNames.Length > 0);
                if (fileNames == null || fileNames.Length < 1)
                    return false;
                Debug.Assert(!String.IsNullOrEmpty(outFile));
                if (String.IsNullOrEmpty(outFile))
                    return false;

                int pageOffset = 0;
                ArrayList master = new ArrayList();
                int f = 0;
                iTextSharp.text.Document document = null;
                PdfCopy writer = null;

                while (f < fileNames.Length)
                {
                    // we create a reader for a certain document
                    PdfReader reader = new PdfReader(fileNames[f]);
                    reader.ConsolidateNamedDestinations();
                    // we retrieve the total number of pages
                    int n = reader.NumberOfPages;
                    pageOffset += n;
                    if (f == 0)
                    {
                        // step 1: creation of a document-object
                        document = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(1));
                        // step 2: we create a writer that listens to the document
                        writer = new PdfCopy(document, new FileStream(outFile, FileMode.Create));
                        // step 3: we open the document
                        document.Open();
                    }
                    // step 4: we add content
                    for (int i = 0; i < n; )
                    {
                        ++i;
                        if (writer != null)
                        {
                            PdfImportedPage page = writer.GetImportedPage(reader, i);
                            writer.AddPage(page);
                        }
                    }
                    PRAcroForm form = reader.AcroForm;
                    if (form != null && writer != null)
                    {
                        //IVAN  writer.CopyAcroForm(reader);
                    }
                    f++;

                    reader.Close();
                }
                // step 5: we close the document
                if (document != null)
                    document.Close();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ExportAllTabsAsPDF(string fileName, TabControl tabControl)
        {
            Debug.Assert(!String.IsNullOrEmpty(fileName));
            if (String.IsNullOrEmpty(fileName))
                return false;
            Debug.Assert(tabControl != null);
            if (tabControl == null)
                return false;

            // lista dei file pdf
            List<String> listFiles = new List<string>();

            for (int i = 0; i < tabControl.Items.Count - 1; i++) // il + lo esclude
            {
                // crea file temp
                String tempFile = Path.GetTempFileName();

                // recupera item
                TabItem item = tabControl.Items[i] as TabItem;

                // il tab item deve essere per forza un controllo canvas
                DCCanvas canvas = (item.Content as DCUserControl).Canvas;

                // esporta pdf
                if (canvas.ExportPdf(tempFile))
                    listFiles.Add(tempFile);
            }

            String mergedFile = Path.GetTempFileName();
            mergedFile = mergedFile.Replace(".tmp", ".pdf");

            // merge della lista
            CombineMultiplePDFs(listFiles.ToArray(), mergedFile);

            // clear file temporanei
            foreach (string csFile in listFiles)
                File.Delete(csFile);

            // mostra il pdf
            Process.Start(mergedFile);

            return true;
        }
    }
}
