﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using DrawControlLibrary.Entities;
using System.Windows.Media.Effects;

namespace DrawControlLibrary.Managers
{
    /// <summary>
    /// Classe manager per lo streaming(es: lettura xml)
    /// </summary>
    internal class DCStreamMngr
    {
        private DCCanvas _canvas;

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="canvas"></param>
        internal DCStreamMngr(DCCanvas canvas)
        {
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            _canvas = canvas;
        }

        internal bool ImportFile(string fileName, bool loadBackground)
        {
            // validazione
            bool isValidFile = DCUtils.CheckFile(fileName);
            if (!isValidFile)
                return false;

            try
            {
                // crea stream e legge il documento
                using (XmlReader reader = XmlReader.Create(fileName))
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            // documento ???
                            #region DCDocument
                            if (reader.Name == "DCDocument")
                            {
                                if (reader.HasAttributes)
                                {
                                    // recupera le proprietà del documento
                                    string val = reader.GetAttribute("sheetWidth");
                                    _canvas.SheetWidth = Convert.ToInt32(val, DCDefinitions.NumberFormat);

                                    val = reader.GetAttribute("sheetHeight");
                                    _canvas.SheetHeight = Convert.ToInt32(val, DCDefinitions.NumberFormat);

                                    val = reader.GetAttribute("sheetColor");
                                    _canvas.SheetColor = DCSupport.GetBrushObject(val);

                                    if (loadBackground)
                                    {
                                        val = reader.GetAttribute("background");
                                        if (DCUtils.CheckFile(val))
                                            _canvas.SetBackground(val);
                                    }

                                    val = reader.GetAttribute("enableMeasureReference");
                                    _canvas.EnableMeasureReference = Convert.ToBoolean(val);

                                    val = reader.GetAttribute("measureReferenceCaption");
                                    _canvas.MeasureReferenceCaption = val;

                                    val = reader.GetAttribute("measureReference");
                                    _canvas.MeasureReference = Convert.ToDouble(val, DCDefinitions.NumberFormat);
                                }
                            }
                            #endregion
                            else
                            {
                                // shape ???
                                #region DCShape
                                if (reader.Name == "DCShape")
                                {
                                    // crea lo shape
                                    DCShape shape = new DCShape();

                                    // recupera le proprietà
                                    string val = reader.GetAttribute("fill");
                                    shape.Fill = DCSupport.GetBrushObject(val);

                                    val = reader.GetAttribute("stroke");
                                    shape.Stroke = DCSupport.GetBrushObject(val);

                                    val = reader.GetAttribute("strokeThickness");
                                    shape.StrokeThickness = Convert.ToDouble(val, DCDefinitions.NumberFormat);

                                    val = reader.GetAttribute("shapeType");
                                    shape.ShapeType = (DCShapeType)Convert.ToInt32(val, DCDefinitions.NumberFormat);

                                    val = reader.GetAttribute("position");
                                    shape.Position = DCSupport.GetPoint(val).Value;

                                    val = reader.GetAttribute("points");
                                    shape.Points = DCSupport.GetPointCollection(val);

                                    val = reader.GetAttribute("scale");
                                    Point scale = DCSupport.GetPoint(val).Value;
                                    shape.Scale = new Point(scale.X, scale.Y);

                                    val = reader.GetAttribute("data");
                                    DCSupport.PutData(val, shape);

                                    val = reader.GetAttribute("id");
                                    shape.Id = val;

                                    val = reader.GetAttribute("smooth");
                                    shape.Smooth = Convert.ToDouble(val, DCDefinitions.NumberFormat);

                                    val = reader.GetAttribute("parameter1");
                                    shape.Parameter1 = Convert.ToDouble(val, DCDefinitions.NumberFormat);

                                    val = reader.GetAttribute("parameter2");
                                    shape.Parameter2 = Convert.ToDouble(val, DCDefinitions.NumberFormat);

                                    val = reader.GetAttribute("parameter3");
                                    shape.Parameter3 = Convert.ToDouble(val, DCDefinitions.NumberFormat);

                                    val = reader.GetAttribute("effect");
                                    shape.Effect = DCSupport.GetEffectObject(val);

                                    val = reader.GetAttribute("showMeasureEnabled");
                                    shape.ShowMeasureEnabled = Convert.ToBoolean(val);

                                    // aggiunge
                                    _canvas.AddEntity(shape, false);
                                }
                                #endregion
                                else
                                    // host e derivati ???
                                    #region Basati su DCHost
                                    if (reader.Name == "DCHost" || reader.Name == "DCImage" || reader.Name == "DCText")
                                    {
                                        // recupera le proprietà comuni
                                        string background = reader.GetAttribute("background");
                                        string borderBrush = reader.GetAttribute("borderBrush");
                                        string borderThickness = reader.GetAttribute("borderThickness");
                                        string position = reader.GetAttribute("position");
                                        string rotation = reader.GetAttribute("rotation");
                                        string strScale = reader.GetAttribute("scale");
                                        Point scale = DCSupport.GetPoint(strScale).Value;
                                        string data = reader.GetAttribute("data");
                                        string id = reader.GetAttribute("id");
                                        string width = reader.GetAttribute("width");
                                        string height = reader.GetAttribute("height");
                                        string strEffect = reader.GetAttribute("effect");
                                        Effect effect = DCSupport.GetEffectObject(strEffect);

                                        // nel caso di un host o un testo leggo anche i dati del font
                                        string foreground = null, fontSize = null, fontFamily = null;
                                        string strHAlign = null, strVAlign = null;
                                        HorizontalAlignment hAlign = HorizontalAlignment.Stretch;
                                        VerticalAlignment vAlign = VerticalAlignment.Stretch;
                                        double dFontSize = 0;

                                        if (reader.Name == "DCHost" || reader.Name == "DCText")
                                        {
                                            foreground = reader.GetAttribute("foreground");
                                            fontSize = reader.GetAttribute("fontSize");
                                            dFontSize = Convert.ToDouble(fontSize, DCDefinitions.NumberFormat);
                                            fontFamily = reader.GetAttribute("fontFamily");
                                            strHAlign = reader.GetAttribute("horizontalContentAlignment");
                                            strVAlign = reader.GetAttribute("verticalContentAlignment");
                                            hAlign = (HorizontalAlignment)Convert.ToInt32(strHAlign, DCDefinitions.NumberFormat);
                                            vAlign = (VerticalAlignment)Convert.ToInt32(strVAlign, DCDefinitions.NumberFormat);
                                        }

                                        // qui viene controllato il dettaglio

                                            #region Dettaglio DCHost
                                            if (reader.Name == "DCHost")
                                            {
                                                DCHost host = new DCHost();

                                                host.Background = DCSupport.GetBrushObject(background);
                                                host.BorderBrush = DCSupport.GetBrushObject(borderBrush);
                                                host.BorderThickness = DCSupport.GetBorderThickness(borderThickness);
                                                host.Position = DCSupport.GetPoint(position).Value;
                                                host.Rotation = Convert.ToDouble(rotation, DCDefinitions.NumberFormat);
                                                host.Scale = new Point(scale.X, scale.Y);
                                                DCSupport.PutData(data, host);
                                                host.Id = id;
                                                host.Width = Convert.ToDouble(width, DCDefinitions.NumberFormat);
                                                host.Height = Convert.ToDouble(height, DCDefinitions.NumberFormat);
                                                host.HorizontalContentAlignment = hAlign;
                                                host.VerticalContentAlignment = vAlign;
                                                host.Effect = effect;

                                                if (!string.IsNullOrEmpty(foreground))
                                                    host.Foreground = DCSupport.GetBrushObject(foreground);

                                                if (dFontSize > 0)
                                                    host.FontSize = dFontSize;

                                                if (!string.IsNullOrEmpty(fontFamily))
                                                    host.FontFamily = new FontFamily(fontFamily);

                                                // acquisisce il content
                                                try
                                                {
                                                    // deve trovare il cdata
                                                    while (reader.Read())
                                                    {
                                                        if (reader.NodeType == XmlNodeType.CDATA)
                                                            break;
                                                    }

                                                    object content = XamlReader.Parse(reader.Value);
                                                    host.Content = content;
                                                }
                                                catch (Exception ex)
                                                {
                                                    Label lblError = new Label();
                                                    lblError.Content = ex.Message;
                                                    host.Content = lblError;
                                                }

                                                // aggiunge
                                                _canvas.AddEntity(host, false);
                                            }
                                            #endregion
                                            else
                                            #region Dettaglio Testo
                                            if (reader.Name == "DCText")
                                            {
                                                string strText = reader.GetAttribute("text");

                                                DCText text = new DCText();

                                                text.Background = DCSupport.GetBrushObject(background);
                                                text.BorderBrush = DCSupport.GetBrushObject(borderBrush);
                                                text.BorderThickness = DCSupport.GetBorderThickness(borderThickness);
                                                text.Position = DCSupport.GetPoint(position).Value;
                                                text.Rotation = Convert.ToDouble(rotation, DCDefinitions.NumberFormat);
                                                text.Scale = new Point(scale.X, scale.Y);
                                                DCSupport.PutData(data, text);
                                                text.Id = id;
                                                text.Width = Convert.ToDouble(width, DCDefinitions.NumberFormat);
                                                text.Height = Convert.ToDouble(height, DCDefinitions.NumberFormat);
                                                text.HorizontalContentAlignment = hAlign;
                                                text.VerticalContentAlignment = vAlign;
                                                text.Effect = effect;

                                                if (!string.IsNullOrEmpty(foreground))
                                                    text.Foreground = DCSupport.GetBrushObject(foreground);

                                                if (dFontSize > 0)
                                                    text.FontSize = dFontSize;

                                                if (!string.IsNullOrEmpty(fontFamily))
                                                    text.FontFamily = new FontFamily(fontFamily);

                                                text.Text = strText;

                                                // aggiunge
                                                _canvas.AddEntity(text, false);
                                            }
                                            #endregion
                                            else
                                            #region Dettaglio Immagine
                                            if (reader.Name == "DCImage")
                                            {
                                                string src = reader.GetAttribute("src");

                                                DCImage image = new DCImage();

                                                image.Background = DCSupport.GetBrushObject(background);
                                                image.BorderBrush = DCSupport.GetBrushObject(borderBrush);
                                                image.BorderThickness = DCSupport.GetBorderThickness(borderThickness);
                                                image.Position = DCSupport.GetPoint(position).Value;
                                                image.Rotation = Convert.ToDouble(rotation, DCDefinitions.NumberFormat);
                                                image.Scale = new Point(scale.X, scale.Y);
                                                DCSupport.PutData(data, image);
                                                image.Id = id;
                                                image.Width = Convert.ToDouble(width, DCDefinitions.NumberFormat);
                                                image.Height = Convert.ToDouble(height, DCDefinitions.NumberFormat);
                                                image.Effect = effect;

                                                if (!String.IsNullOrEmpty(src))
                                                    image.Src = new BitmapImage(new Uri(src));

                                                // aggiunge
                                                _canvas.AddEntity(image, false);
                                            }
                                            #endregion
                                    }
                                    #endregion
                            }
                        }
                    } // end while

                    reader.Close();
                } // end using

                return true;
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);

                return false;
            }
        }
    }
}
