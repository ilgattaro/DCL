﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DrawControlLibrary.Managers
{
    /// <summary>
    /// Manager della selezione singola o multipla(eventi e codice)
    /// </summary>
    internal static class DCSelectionMngr
    {
        #region Eventi
        internal static void OnMouseDown(DCCanvas canvas, MouseButtonEventArgs e)
        {
            // non viene gestito il tasto destro
            if (e.ChangedButton == MouseButton.Right)
                return;

            // ottiene hit
            HitTestResult hitTestResult = VisualTreeHelper.HitTest(canvas, canvas._oldMousePosition);

            // recupera l'ultima entità catturata(se esiste)
            IDCEntity entity = canvas._lastCapturedEntity;

            if (entity != null)
            {
                // itera con l'entità toccata
                DCBasicInteractionMngr.OnMouseDown(entity, e);

                // agisce sulla selezione se è selezionata
                if (entity.Selected)
                    if (canvas.Selection.Count > 1)
                    {
                        foreach (IDCEntity entitySelected in canvas.Selection)
                            DCBasicInteractionMngr.OnMouseDown(entitySelected, e);
                    }
            }

            // verifica di poter ripreparare il rettangolo di selezione a patto che il mouse stia toccando un oggetto diverso da un IDCEntity
            if (hitTestResult != null && hitTestResult.VisualHit != null && (hitTestResult.VisualHit is Grid || hitTestResult.VisualHit is Rectangle || hitTestResult.VisualHit is Image))
                ResetSelectionBox(canvas);
        }

        internal static void OnMouseMove(DCCanvas canvas, MouseEventArgs e)
        {
            // recupero le coordinate
            Point mousePosition = e.GetPosition(canvas);

            // recupera l'ultima entità catturata(se esiste)
            IDCEntity entity = canvas._lastCapturedEntity;

            if (entity != null)
            {
                // itera sull'entità toccata
                DCBasicInteractionMngr.OnMouseMove(entity, e);
                
                // agisce sulla selezione se è selezionata
                if (entity.Selected)
                    if (canvas.Selection.Count > 1)
                    {
                        foreach (IDCEntity entitySelected in canvas.Selection)
                            DCBasicInteractionMngr.OnMouseMove(entitySelected, e);
                    }

                // store della posizione(necessaria)
                canvas._oldMousePosition = mousePosition;
            }

            // gestione del rettangolo di selezione se il canvas ha un "drag vuoto" in corso
            // (verifica se può essere mostrato il rettangolo) e notifica il flag "selection"

            if (canvas._enableSelectionBox && canvas._captured && canvas._selectionBox.Visibility == Visibility.Visible)
            {
                // calcola delta di ridimensionamento
                double deltaX = canvas._oldMousePosition.X - mousePosition.X;
                double deltaY = canvas._oldMousePosition.Y - mousePosition.Y;

                // se c'è un delta attiva il flag di selezione
                if (deltaX != 0 && deltaY != 0)
                    canvas._selection = true;

                // mostra grafica del rettangolo
                DCSelectionMngr.UpdateSelectionBox(canvas, e);
            }
        }

        internal static void OnMouseUp(DCCanvas canvas, MouseButtonEventArgs e)
        {            
            // non viene gestito il tasto destro
            if (e.ChangedButton==MouseButton.Right)
                return;

            // ottiene hit
            HitTestResult hitTestResult = VisualTreeHelper.HitTest(canvas, canvas._oldMousePosition);

            // recupera l'ultima entità catturata(se esiste)
            IDCEntity entity = canvas._lastCapturedEntity;

            if (entity != null)
            {
                // liste temporanee per la rimozione o aggiunta alla selezione
                List<IDCEntity> selectionToRemove = new List<IDCEntity>();
                List<IDCEntity> selectionToAdd = new List<IDCEntity>();

                // itera sull'entità toccata
                DCBasicInteractionMngr.OnMouseUp(entity, e);

                // c'era un drag in corso sull'entità toccata? se no inverte selezione
                if ((bool)entity.Data[DCDefinitions.KEY_SYS_DRAGGING] == false)
                {
                    // verifica tasto shift
                    bool shift = Keyboard.IsKeyDown(Key.LeftShift);
                    if (!shift)
                        canvas.UnSelectAll();

                    if (entity.Selected)
                        selectionToRemove.Add(entity);
                    else
                        selectionToAdd.Add(entity);
                }

                // agisce sulla selezione se è selezionata
                if (entity.Selected)
                    if (canvas.Selection.Count > 1)
                    {
                        foreach (IDCEntity entitySelected in canvas.Selection)
                            DCBasicInteractionMngr.OnMouseUp(entitySelected, e);
                    }

                // agisco solo dopo sulla lista della selezione perchè può variare e causerebbe un eccezione se la modifico dal foreach
                foreach (IDCEntity entToRemove in selectionToRemove)
                    entToRemove.Selected = false;
                foreach (IDCEntity entToAdd in selectionToAdd)
                    entToAdd.Selected = true;
            }

            // gestisce la selezione degli elementi toccati dal rettangolo
            if (canvas._captured && canvas._selection && canvas._enableSelectionBox)
                TryMakeSelection(canvas);
        }
        #endregion

        /// <summary>
        /// Muove la selezione
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="offsetX"></param>
        /// <param name="offsetY"></param>
        internal static void PanSelection(DCCanvas canvas, double offsetX, double offsetY)
        {  
            // validazione canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // esegue comando contestuale sulla selezione
            DCCommandContextMngr.ExecuteContestualCommand(canvas, delegate(IDCEntity entity){ entity.Position = new Point(entity.Position.X + offsetX, entity.Position.Y + offsetY); }, null, false);
        }

        /// <summary>
        /// Prova a creare la selezione
        /// </summary>
        /// <param name="canvas"></param>
        internal static void TryMakeSelection(DCCanvas canvas)
        {
            // validazione canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // costruisco il rettangolo di selezione
            float boxLeft = (float)DCCanvas.GetLeft(canvas._selectionBox);
            float boxTop = (float)DCCanvas.GetTop(canvas._selectionBox);
            float boxWidth = (float)canvas._selectionBox.Width;
            float boxHeight = (float)canvas._selectionBox.Height;
            System.Drawing.RectangleF boxSelection = new System.Drawing.RectangleF(boxLeft, boxTop, boxWidth, boxHeight);

            // creo una lista degli elementi selezionati
            List<IDCEntity> selectionList = new List<IDCEntity>();

            // scorro tutti i figli del canvas
            foreach (FrameworkElement child in canvas.Children)
            {
                // verifico se è un entità valida
                if (child is IDCEntity)
                {
                    // recupero entità e verifico intersezione, se interseca inserisco nella lista della selezione
                    IDCEntity entity = child as IDCEntity;

                    if (boxSelection.IntersectsWith(entity.GetBounds()))
                        selectionList.Add(entity);
                }
            }

            // gli elementi della lista vanno selezionati
            foreach (IDCEntity entity in selectionList)
                entity.Selected = !entity.Selected;

            // genera l'evento di selezione rettangolare(se definito)
            canvas.FireRectangularSelectionEvent(boxSelection);

            // pulizia
            selectionList.Clear();
            selectionList = null;
            GC.Collect();
        }

        /// <summary>
        /// Ripristina il box di selezione
        /// </summary>
        /// <param name="canvas"></param>
        internal static void ResetSelectionBox(DCCanvas canvas)
        {
            // validazione canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // riaggiungo il rettangolo e faccio i settaggi
            if (canvas._selectionBox.Visibility != Visibility.Visible)
            {
                // aggiungo al canvas
                canvas.Children.Add(canvas._selectionBox);

                // settaggi
                canvas._selectionBox.Width = 0;
                canvas._selectionBox.Height = 0;
                DCCanvas.SetLeft(canvas._selectionBox, canvas._oldMousePosition.X);
                DCCanvas.SetTop(canvas._selectionBox, canvas._oldMousePosition.Y);
                canvas._selectionBox.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Aggiorna il box di selezione
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="e"></param>
        internal static void UpdateSelectionBox(DCCanvas canvas, MouseEventArgs e)
        {
            // validazione canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // recupera mouse
            Point mousePosition = e.GetPosition(canvas);

            // gestione del ridimensionamento
            if (canvas._oldMousePosition.X < mousePosition.X)
            {
                DCCanvas.SetLeft(canvas._selectionBox, canvas._oldMousePosition.X);
                canvas._selectionBox.Width = mousePosition.X - canvas._oldMousePosition.X;
            }
            else
            {
                DCCanvas.SetLeft(canvas._selectionBox, mousePosition.X);
                canvas._selectionBox.Width = canvas._oldMousePosition.X - mousePosition.X;
            }

            if (canvas._oldMousePosition.Y < mousePosition.Y)
            {
                DCCanvas.SetTop(canvas._selectionBox, canvas._oldMousePosition.Y);
                canvas._selectionBox.Height = mousePosition.Y - canvas._oldMousePosition.Y;
            }
            else
            {
                DCCanvas.SetTop(canvas._selectionBox, mousePosition.Y);
                canvas._selectionBox.Height = canvas._oldMousePosition.Y - mousePosition.Y;
            }
        }

        /// <summary>
        /// Rimuove il box di selezione
        /// </summary>
        /// <param name="canvas"></param>
        internal static void ClearSelectionBox(DCCanvas canvas)
        {
            // validazione canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // contrassegna come nascosto(necessario)
            canvas._selectionBox.Visibility = Visibility.Hidden;

            // rimuove anche dal canvas(la futura riaggiunta lo porta in primo piano)
            canvas.Children.Remove(canvas._selectionBox);
        }
    }
}
