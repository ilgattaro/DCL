﻿using System;
using System.Windows;

namespace DrawControlLibrary.Managers
{
    /// <summary>
    /// Classe manager per la gestione dell'errore
    /// </summary>
    public static class DCErrorMngr
    {
        /// <summary>
        /// Ottiene l'ultimo errore registrato
        /// </summary>
        public static Exception LastError { get; private set; }

        /// <summary>
        /// Ottiene o imposta il callback registrato quando si verifica un errore
        /// </summary>
        public static Action<Exception> ErrorHandler { get; set; }

        /// <summary>
        /// Gestisce l'errore e chiama il callback
        /// </summary>
        /// <param name="ex"></param>
        public static void HandleEx(Exception ex, bool callback = true)
        {
            LastError = ex;

            if (ex == null)
                return;

            MessageBox.Show(ex.Message + "\n" + (ex.InnerException != null ? ex.InnerException.Message + "\n" : "") + ex.StackTrace, "DrawControlLibrary", MessageBoxButton.OK, MessageBoxImage.Error);

            if (ErrorHandler != null && callback)
                ErrorHandler(ex);
        }
    }
}
