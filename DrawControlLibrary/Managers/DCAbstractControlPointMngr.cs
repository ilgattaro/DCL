﻿using DrawControlLibrary.Adorners;
using DrawControlLibrary.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace DrawControlLibrary.Managers
{
    /// <summary>
    /// Classe astratta manager per i punti di controllo
    /// </summary>
    public abstract class DCAbstractControlPointMngr
    {
        protected IDCEntity _entity;

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="entity"></param>
        public DCAbstractControlPointMngr(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // ricorda l'entità settata
            _entity = entity;

            // recupera canvas
            DCCanvas canvas = _entity.GetCanvas();

            // valida canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // aggiunge i punti al canvas
            AddPoints();
        }

        /// <summary>
        /// Aggiunge i punti di controllo al canvas
        /// </summary>
        public virtual void AddPoints()
        {
            // recupera canvas
            DCCanvas canvas = _entity.GetCanvas();

            // valida canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // mostra punti di controllo con l'adorner
            if(_entity is DCHost)
                canvas._adornerLayer.Add(new ResizeRotateAdorner(_entity as DCHost));
        }

        /// <summary>
        /// Cancella i punti dal canvas
        /// </summary>
        public void Clear()
        {
            List<DCControlPoint> listCPoints = new List<DCControlPoint>();
            
            // recupera il canvas
            DCCanvas canvas = _entity.GetCanvas();

            // valida canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // rimuove l'adorner
            Adorner[] adorners = canvas._adornerLayer.GetAdorners(_entity as UIElement);
            if(adorners != null && adorners.Length > 0)
                foreach(Adorner adorner in adorners)
                    canvas._adornerLayer.Remove(adorner);

            // aggiungo alla lista dei punti da cancellare
            foreach (DCControlPoint cp in canvas._cacheControlPoints)
            {
                if (cp._entity == _entity)
                    listCPoints.Add(cp);
            }

            // rimuovo dal canvas
            foreach (DCControlPoint cp in listCPoints)
            {
                canvas.Children.Remove(cp);
                canvas._cacheControlPoints.Remove(cp);
            }

            // pulizia della lista
            listCPoints.Clear();
            GC.Collect();
        }

        /// <summary>
        /// Aggiorna i punti di controllo in corrispondenza dell'entità
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Rigenera i punti di controllo in corrispondenza dell'entità
        /// </summary>
        public void Regen()
        {
            // rimuove e riaggiunge
            Clear();
            AddPoints();
            
            GC.Collect();
        }
    }
}