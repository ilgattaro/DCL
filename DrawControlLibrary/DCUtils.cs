﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DrawControlLibrary
{
    /// <summary>
    /// Metodi e proprietà di Utility "pubbliche"
    /// </summary>
    public static class DCUtils
    {
        public static double DPIX { get; private set; }
        public static double DPIY { get; private set; }
        
        static DCUtils()
        {
            System.Drawing.Graphics g = System.Drawing.Graphics.FromHwnd(IntPtr.Zero);
            Debug.Assert(g != null);
            if (g != null)
            {
                DPIX = g.DpiX;
                DPIY = g.DpiY;
            }
        }

        public static Point ZeroPt() { return new Point(0, 0); }

        public static System.Drawing.RectangleF GetRectangleF(this Rect rect)
        {
            return new System.Drawing.RectangleF(
                (float)rect.X,
                (float)rect.Y,
                (float)rect.Width,
                (float)rect.Height
                );
        }

        public static Rect GetRect(this System.Drawing.RectangleF rectanglef)
        {
            return new Rect(rectanglef.X, rectanglef.Y, rectanglef.Width, rectanglef.Height);
        }

        public static Point GetClone(this Point point)
        {
            return new Point(point.X, point.Y);
        }

        public static Size GetClone(this Size size)
        {
            return new Size(size.Width, size.Height);
        }

        public static double GetDistance(this Point p, Point q)
        {
            Debug.Assert(q != null);
            if (q == null)
                return 0;

            double a = p.X - q.X;
            double b = p.Y - q.Y;
            double distance = Math.Sqrt(a * a + b * b);
            return distance;
        }

        public static double ToRadians(double angle)
        {
            return Math.PI * angle / 180;
        }

        public static IDCEntity FindIDCEntityParent(DependencyObject child)
        {
            Debug.Assert(child != null);
            if (child == null)
                return null;

            DependencyObject parentObject = VisualTreeHelper.GetParent(child);
            if (parentObject == null)
                return null;

            IDCEntity parent = parentObject as IDCEntity;
            if (parent != null)
                return parent;
            else
                return FindIDCEntityParent(parentObject);
        }

// metodi vecchi deprecati
        /*
        public static Color GetColorFromBrush(Brush brush)
        {
            if (brush == null || !(brush is SolidColorBrush))
                return Colors.Black;
            string stringColor = brush.ToString();
            Color color = (Color)ColorConverter.ConvertFromString(stringColor);
            return color;
        }

        public static Brush GetBrushFromColor(Color color)
        {
            if (color == null)
                return Brushes.Black;

            return new SolidColorBrush(color);
        }

        public static Brush GetBrushFromString(string strColor)
        {
            if (String.IsNullOrEmpty(strColor))
                return Brushes.Transparent;

            try
            {
                BrushConverter converter = new BrushConverter();
                Brush brush = (Brush)converter.ConvertFromString(strColor);
                return brush;
            }
            catch
            {
                return Brushes.Black;
            }
        }*/

        public static Point GetScaledPoint(Point pt, double factorX, double factorY)
        {
            Debug.Assert(pt != null);
            if (pt == null)
                return ZeroPt();
            return new Point(pt.X / factorX , pt.Y / factorY);
        }
        
        public static double GetDPI()
        {
            System.Drawing.Graphics g = System.Drawing.Graphics.FromHwnd(IntPtr.Zero);
            return g.DpiX;
        }

        /*
        public static Size GetElementPixelSize(FrameworkElement element, Size size)
        {
            Matrix transformToDevice;
            var source = PresentationSource.FromVisual(element);
            if (source != null)
                transformToDevice = source.CompositionTarget.TransformToDevice;
            else
                using (var source2 = new HwndSource(new HwndSourceParameters()))
                    transformToDevice = source.CompositionTarget.TransformToDevice;

            return (Size)transformToDevice.Transform((Vector)size);
        }
        */

        public static Size? GetDPIWpf(FrameworkElement element)
        {
            double dpiX = DPIX;
            double dpiY = DPIY;

            PresentationSource source = PresentationSource.FromVisual(element);
            if (source != null)
            {
                dpiX = dpiX * source.CompositionTarget.TransformToDevice.M11;
                dpiY = dpiY * source.CompositionTarget.TransformToDevice.M22;
            }

            return new Size(dpiX, dpiY);
        }

        public static double GetPxByCm(double cm)
        {
            double px = 0;
            px = cm * DPIX / 2.54;
            return px;
        }

        public static System.Drawing.Size? GetImageSize(string fileName)
        {
            // validazione
            bool isValidFile = CheckFile(fileName);
            Debug.Assert(isValidFile);
            if (!isValidFile)
                return null;

            try
            {
            System.Drawing.Image img = new System.Drawing.Bitmap(fileName);
            return img.Size;
            }
            catch
            {
                return null;
            }
        }

        public static bool CheckFile(string fileName)
        {
            bool emptyFile = String.IsNullOrEmpty(fileName);
            //Debug.Assert(!emptyFile);
            if (emptyFile)
                return false;

            bool existFile = File.Exists(fileName);
            Debug.Assert(existFile);
            if (!existFile)
                return false;

            return true;
        }

        public static double CalcScale(double val, double factor)
        {
            if (factor == 0)
                return val;
            return (val > 0) ? (val * factor) : (val / factor);
        }

        public static bool SaveImage(BitmapSource image, String fileName)
        {
            Debug.Assert(image != null);
            if (image == null)
                return false;

            bool emptyFile = String.IsNullOrWhiteSpace(fileName);
            Debug.Assert(!emptyFile);
            if (emptyFile)
                return false;

            try
            {
                using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    PngBitmapEncoder imgEncoder = new PngBitmapEncoder();
                    imgEncoder.Frames.Add(BitmapFrame.Create(image));
                    imgEncoder.Save(stream);

                    stream.Close();
                    stream.Dispose();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void StartObjectDrag(DependencyObject dragSource, string data)
        {
            // validazione
            Debug.Assert(dragSource != null);
            if (dragSource == null)
                return;

            bool isEmptyData=string.IsNullOrEmpty(data);
            Debug.Assert(!isEmptyData);
            if (isEmptyData)
                return;

            DataObject dataObj = new DataObject(data, dragSource);
            DragDrop.DoDragDrop(dragSource, dataObj, DragDropEffects.Move);
        }

        public static BitmapImage GetImage(string fileName)
        {
            // validazione
            if (!fileName.Contains(DCDefinitions.IMAGE_PATH))
            {
                bool isValidFile = CheckFile(fileName);
                Debug.Assert(isValidFile);
                if (!isValidFile)
                    return null;
            }

            try
            {
                return new BitmapImage(new Uri(fileName, UriKind.RelativeOrAbsolute));
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            // get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            // we've reached the end of the tree
            if (parentObject == null) return null;

            // check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }
    }
}
