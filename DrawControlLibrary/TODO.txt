﻿TODO LIST
---------

- copia e incolla
- scala selezione
- tools di disegno e vari
- undo redo
- drag esterno and drop
- provvedere quando c'è la selezione di un oggetto un punto di controllo per la modifica della rotazione e uno per la scala ( nel caso degli user control la scala non è necessaria )
- introdurre filtri
- snap e grid lines
- show hide unità di riferimento
- fixare unità di riferimento
- raster gruppo
- introdurre brush editor ( nell'app )
- salvare immagini rasterizzate quando salva il documento
- punti di controllo vincolati( come per le entità anche i punti di controllo possono essere spostati solo in determinate direzioni )
- terminare la serializzazione degli effetti