﻿namespace DrawControlLibrary
{
    public enum DCShapeType
    {
        Line,
        Ellipse,
        Polyline,
        Polygon,
        Bezier,
        Arrow,
        CurvedPolygon,
        Rectangle,
        CloudPolygon
    }

    public enum DCMoveableType
    {
        MoveableFree,
        MoveableHorizontal,
        MoveableVertical,
        NoMoveable
    }

    public enum DCSelectionType
    {
        Rectangular,
        Clicked,
        Code
    }
}
