﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary
{
    /// <summary>
    /// punto di controllo che modifica un punto dell'entità
    /// </summary>
    internal class DCControlPoint : Thumb
    {
        internal int _index;
        internal IDCEntity _entity;

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="index"></param>
        internal DCControlPoint(IDCEntity entity, int index)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            Debug.Assert(index >= 0);
            if (index < 0)
                throw new ArgumentOutOfRangeException("index");

            _entity = entity;
            _index = index;

            DragDelta += new DragDeltaEventHandler(DCControlPoint_DragDelta);

            // aggiungo alla cache
            DCCanvas canvas = _entity.GetCanvas();
            canvas._cacheControlPoints.Add(this);
        }

        /// <summary>
        /// Ottiene il punto di snap
        /// </summary>
        /// <returns></returns>
        internal Point GetSnapPoint()
        {
            if (_entity is DCShape)
                return (_entity as DCShape).GetAbsolutePoint(_index);

            // gestire gli altri tipi
            var bounds = _entity.GetBounds();
            return new Point(_entity.Position.X + bounds.Width, _entity.Position.Y + bounds.Height);
        }

        private void DCControlPoint_DragDelta(object sender, DragDeltaEventArgs e)
        {
            // recupero posizionamento entità
            Point position = _entity.Position;

            // se si tratta di uno shape
            if (_entity is DCShape)
            {
                DCShape shape = _entity as DCShape;

                // recupero punto iesimo associato al punto di controllo
                Point p = shape.Points[_index];

                // aggiorna il punto dell'entità
                p.X += e.HorizontalChange;
                p.Y += e.VerticalChange;

                shape.Points[_index] = p;

                // aggiorna se stesso
                DCCanvas.SetLeft(this, position.X + p.X);
                DCCanvas.SetTop(this, position.Y + p.Y);
            }
            else if (_entity is DCHost)
            {
                DCHost host = _entity as DCHost;

                // aggiorna dimensioni width e height
                double w = host.ActualWidth + e.HorizontalChange;
                w = w >= 0 ? w : 0;
                host.Width = w;
                double h = host.ActualHeight + e.VerticalChange;
                h = h >= 0 ? h : 0;
                host.Height = h;

                // aggiorna layout(necessario farlo qui)
                host.UpdateLayout();

                // aggiorna se stesso
                DCCanvas.SetLeft(this, position.X + host.ActualWidth);
                DCCanvas.SetTop(this, position.Y + host.ActualHeight);

                // fire del change di proprietà
                host.FireChanges();
            }

            // notifica edit
            _entity._notifyEditEvent(_index);
        }

        protected override void OnMouseUp(System.Windows.Input.MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);

            DCCanvas canvas = _entity.GetCanvas();

            // verifico il canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            DCControlPoint found = null;
            Point? a = null, b = null;

            // cicla i punti nella cache e trova il punto con cui snappare
            foreach (DCControlPoint cp in canvas._cacheControlPoints)
            {
                // ignora il punto medesimo
                if (this == cp)
                    continue;

                // ottiene punti di aggancio
                a = this.GetSnapPoint();
                b = cp.GetSnapPoint();

                // calcola distanza e verifica se la distanza è accettabile per lo snap
                if (a.Value.GetDistance(b.Value) <= 9)
                {
                    found = cp;
                    break;
                }
            }

            // se non trova il punto esce
            if (found == null)
                return;

            // aggiorna posizione del punto di controllo
            Point position = new Point(DCCanvas.GetLeft(found), DCCanvas.GetTop(found));
            DCCanvas.SetLeft(this, position.X);
            DCCanvas.SetTop(this, position.Y);

            // azione da eseguire in base all'entità
            if (_entity is DCShape)
            {
                (_entity as DCShape).SetFromAbsolutePoint(this._index, b.Value.GetClone());
            }
            else if (_entity is DCHost)
            {
                (_entity as DCHost).Width = position.X - _entity.Position.X;
                (_entity as DCHost).Height = position.Y - _entity.Position.Y;
            }

            //aggiorna scroll
            canvas.InvalidateMeasure();
        }
    }
}
