﻿using System.Windows;
using System.Windows.Input;
using DrawControlLibrary.Entities;
using System;

namespace DrawControlLibrary.Tools
{
    /// <summary>
    /// Strumento di sistema per il cambio dell'unità di riferimento
    /// </summary>
    public class DCMeasureTool : DCAbstractTool
    {
        int step = 0;

        public override void Start(object param = null)
        {
            base.Start(param);

            DCToolHelper.SetupShape(Canvas.ToolShape_1);
            Canvas.ToolShape_1.ShapeType = DCShapeType.Line;
            Canvas.SetPrompt("Cliccare su due punti per tracciare la distanza di riferimento");
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            if (IsAbort)
                return;

            Point mouse = Mouse.GetPosition(Canvas);

            if (step == 0)
            {
                Canvas.ToolShape_1.SetStartPoint(mouse);
                
            }
            else if (step == 1)
            {
                // setta il secondo punto e aggiunge la linea
                Canvas.ToolShape_1.SetFromAbsolutePoint(1, mouse);

                // setta l'unità di riferimento
                DCMeasureInputDlg dlg = new DCMeasureInputDlg();
                if (dlg.ShowDialog().Value)
                {
                    double dist = DCMathUtils.GetDistance(Canvas.ToolShape_1.Points[0].X, Canvas.ToolShape_1.Points[0].Y, Canvas.ToolShape_1.Points[1].X, Canvas.ToolShape_1.Points[1].Y);
                    Canvas.MeasureReference = dist / dlg.Measure;
                }

                // refresh prompt
                Canvas.RefreshPrompt();
                Canvas.SetPrompt("Cliccare su due punti per tracciare la distanza di riferimento");

                // rifa il setup
                Canvas.ToolShape_1.SetStartPoint(mouse);
                Canvas.ToolShape_1.SetFromAbsolutePoint(1, mouse);

                step = 0;
                return;
            }
            
            step++;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (step > 0)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Point start = Canvas.ToolShape_1.Points[0];
                Canvas.ToolShape_1.Points[1] = new Point(mouse.X - Canvas.ToolShape_1.Position.X, mouse.Y - Canvas.ToolShape_1.Position.Y);

                double dist = DCMathUtils.GetDistance(start.X, start.Y, Canvas.ToolShape_1.Points[1].X, Canvas.ToolShape_1.Points[1].Y);
                Canvas.SetPrompt(String.Format("{0:0.00}",dist) + " px");
            }
        }
    }
}
