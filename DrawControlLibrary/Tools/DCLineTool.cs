﻿using System.Windows;
using System.Windows.Input;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary.Tools
{
    /// <summary>
    /// Strumento di disegni di linee
    /// </summary>
    public class DCLineTool : DCAbstractTool
    {
        int step = 0;

        public override void Start(object param = null)
        {
            base.Start(param);

            DCToolHelper.SetupShape(Canvas.ToolShape_1);
            Canvas.ToolShape_1.ShapeType = DCShapeType.Line;
            Canvas.SetPrompt("Primo punto");
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            if (IsAbort)
                return;

            Point mouse = Mouse.GetPosition(Canvas);

            if (step == 0)
            {
                Canvas.ToolShape_1.SetStartPoint(mouse);
                Canvas.SetPrompt("Secondo punto");
            }
            else if (step == 1)
            {
                // setta il secondo punto e aggiunge la linea
                Canvas.ToolShape_1.SetFromAbsolutePoint(1, mouse);

                // aggiunge linea
                DCShape line = new DCShape();
                    line.Position = Canvas.ToolShape_1.Position.GetClone();
                    line.Points.Add(new Point(0, 0));
                    line.Points.Add(Canvas.ToolShape_1.Points[1].GetClone());
                    Canvas.AddEntity(line);

                // refresh prompt
                Canvas.RefreshPrompt();
                Canvas.SetPrompt("Primo punto");

                // rifa il setup
                Canvas.ToolShape_1.SetStartPoint(mouse);
                Canvas.ToolShape_1.SetFromAbsolutePoint(1, mouse);

                step = 0;
                return;
            }
            
            step++;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (step > 0)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Canvas.ToolShape_1.Points[1] = new Point(mouse.X - Canvas.ToolShape_1.Position.X, mouse.Y - Canvas.ToolShape_1.Position.Y);
            }
        }
    }
}
