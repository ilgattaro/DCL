﻿using System.Windows;
using System.Windows.Input;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary.Tools
{
    /// <summary>
    /// Strumento di inserimento testi
    /// </summary>
    public class DCTextTool : DCAbstractTool
    {
        int step = 0;

        public override void Start(object param = null)
        {
            base.Start(param);

            DCToolHelper.SetupShape(Canvas.ToolShape_1);
            Canvas.ToolShape_1.ShapeType = DCShapeType.Rectangle;
            Canvas.SetPrompt("Primo punto");
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            if (IsAbort)
                return;

            if (step == 0)
            {
                Canvas.ToolShape_1.Position = Mouse.GetPosition(Canvas);
                Canvas.SetPrompt("Secondo punto");
            }

            else if (step == 1)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Canvas.ToolShape_1.SetFromAbsolutePoint(1, mouse);

                DCText text = new DCText();
                    text.Position = Canvas.ToolShape_1.Position.GetClone();
                    text.Width = Canvas.ToolShape_1.Points[1].X + 3; // aggiunge offset di focus
                    text.Height = Canvas.ToolShape_1.Points[1].Y + 3; // aggiunge offset di focus
                    text.BorderThickness = new Thickness(0);
                    text.FontFamily = new System.Windows.Media.FontFamily("Tahoma");
                    text.FontSize = 20;
                
                Canvas.AddEntity(text);

                Finish();

                return;
            }

            step++;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (step == 1)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Canvas.ToolShape_1.Points[1] = new Point(mouse.X - Canvas.ToolShape_1.Position.X, mouse.Y - Canvas.ToolShape_1.Position.Y);
            }
        }
    }
}
