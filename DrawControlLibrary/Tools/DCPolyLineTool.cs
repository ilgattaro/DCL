﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrawControlLibrary.Tools
{
    /// <summary>
    /// Strumento di disegno delle polilinee
    /// </summary>
    public class DCPolyLineTool : DCPolygonTool
    {
        /// <summary>
        /// Costruttore
        /// </summary>
        public DCPolyLineTool()
        {
            _shapeType = DCShapeType.Polyline;
        }
    }
}
