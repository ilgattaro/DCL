﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrawControlLibrary.Tools
{
    /// <summary>
    /// Strumento di disegno nuvole
    /// </summary>
    public class DCCloudPolygonTool : DCPolygonTool
    {
        /// <summary>
        /// Costruttore
        /// </summary>
        public DCCloudPolygonTool()
        {
            _shapeType = DCShapeType.CloudPolygon;
        }
    }
}
