﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary.Tools
{
    /// <summary>
    /// Strumento di inserimento immagini
    /// </summary>
    public class DCImageTool : DCAbstractTool
    {
        private string _imageFileName;
        double _imageWidth;
        double _imageHeight;
        private ImageSource _image;
        Rect _imageRect;

        public override void Start(object param = null)
        {
            base.Start(param);

            OpenFileDialog dialog=new OpenFileDialog();
            dialog.Filter="Immagini|*.png;*.jpg;*.jpeg;*.bmp;*.gif";
            dialog.CheckFileExists=true;
            dialog.CheckPathExists=true;
            if (!dialog.ShowDialog().Value)
            {
                IsAbort = true;
                return;
            }

            // ottiene le informazioni dell'immagine
                _imageFileName = dialog.FileName;

                var imageSize = DCUtils.GetImageSize(_imageFileName);
                    _imageHeight = imageSize.Value.Height;
                    _imageWidth = imageSize.Value.Width;

                _image = new BitmapImage(new Uri(_imageFileName));

                _imageRect = new Rect(0, 0, _imageWidth, _imageHeight);

            // inizializza shape di contorno
            DCToolHelper.SetupShape(Canvas.ToolShape_1);
            Canvas.ToolShape_1.ShapeType = DCShapeType.Rectangle;
            Canvas.ToolShape_1.RenderEvent += new RenederDelegate(Sys_Shape_1_RenderEvent);

            Canvas.SetPrompt("Punto in cui inserire l'immagine");
        }

        void Sys_Shape_1_RenderEvent(DrawingContext dc)
        {
            dc.DrawImage(_image,_imageRect);
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            if (IsAbort)
                return;

            Point mouse = Mouse.GetPosition(Canvas);

            DCImage image  = new DCImage();
            image.Position = mouse;
            image.Width    = _imageWidth;
            image.Height   = _imageHeight;
            image.Src=new BitmapImage(new Uri(_imageFileName));

            Canvas.AddEntity(image);

            // refresh dell'immagine
            Canvas.Children.Remove(Canvas.ToolShape_1);
            Canvas.Children.Add(Canvas.ToolShape_1);
            
            // refresh del prompt
            Canvas.RefreshPrompt();
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (String.IsNullOrWhiteSpace(_imageFileName))
                return;

            Point mouse = Mouse.GetPosition(Canvas);

            // aggiorna la posizione dell'immagine di anteprima
            Canvas.ToolShape_1.Position = mouse;
            Canvas.ToolShape_1.Points[0] = new Point(0, 0);
            Canvas.ToolShape_1.Points[1] = new Point(_imageWidth, _imageHeight);
        }

        public override void Finish()
        {
            base.Finish();

            // libero l'evento
            Canvas.ToolShape_1.RenderEvent -= Sys_Shape_1_RenderEvent;

            _image = null;

            GC.Collect();
        }
    }
}
