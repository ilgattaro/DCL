﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using DrawControlLibrary.Managers;
using DrawControlLibrary.Entities;
using System.Windows.Media.Effects;

namespace DrawControlLibrary
{ 
    /// <summary>
    /// Core di funzionamento del canvas, contiene procedure ed eventi di varie sorgenti
    /// </summary>
    internal static class DCCore
    {

        #region Procedure
        // identifica il metodo chiamante(usato dalla selezione)
        private static DCSelectionType _identifySelectionMethod()
        {
            StackTrace stackTrace = new StackTrace();
            StackFrame[] stackFrames = stackTrace.GetFrames();

            int i = 0;
            int max = 25;
            foreach (StackFrame stackFrame in stackFrames)
            {
                string method = stackFrame.GetMethod().Name;

                if (method == "TryMakeSelection")
                    return DCSelectionType.Rectangular;
                else if (method == "OnMouseUp")
                    return DCSelectionType.Clicked;
                i++;

                if (i == max)
                    return DCSelectionType.Code;
            }
            return DCSelectionType.Code;
        }

        // enfatizza entità
        private static void _addBlink(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            bool blinked = (bool)entity.Data[DCDefinitions.KEY_BLINKED];
            if (blinked)
                return;

            // recupera elemento ui
            FrameworkElement element = entity as FrameworkElement;

            Debug.Assert(element != null);
            if (element == null)
                throw new ArgumentNullException("element");

            // rende semi trasparente
            element.Opacity = 0.75;

            // flag blink
            entity.Data[DCDefinitions.KEY_BLINKED] = true;
        }

        // disabilità enfasi
        private static void _removeBlink(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            bool blinked = (bool)entity.Data[DCDefinitions.KEY_BLINKED];
            if (!blinked)
                return;

            // recupera elemento ui
            FrameworkElement element = entity as FrameworkElement;

            Debug.Assert(element != null);
            if (element == null)
                throw new ArgumentNullException("element");

                // ripristina opacità
                element.Opacity = 1;

                // flag blink
                entity.Data[DCDefinitions.KEY_BLINKED] = false;
        }

        // ottiene la tipologia di oggetto draggato
        private static void _parseDragFormat(DragEventArgs e, out object dataObj, out string format)
        {
            Debug.Assert(e != null);
            if (e == null)
                throw new ArgumentNullException("e");

            // verifico il tipo di oggetto che è stato draggato
            string[] formats = e.Data.GetFormats();

            bool formatCondiction = formats != null && formats.Length > 0;
            Debug.Assert(formatCondiction);
            if (!formatCondiction)
                throw new Exception("Invalid drag format");

            // ottiene il primo formato
            format = formats[0];

            // ottiene il dato draggato
            dataObj = e.Data.GetData(format);
        }

        // chiamato quando la proprietà select dell'entità è modificata
        internal static void OnSelect(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            IDCEntity entity = dependencyObject as IDCEntity;
            bool? select = e.NewValue as bool?;

            // non succede nulla se il canvas è null
            DCCanvas canvas = entity.GetCanvas();
            if (canvas == null)
                return;

            // verifica che selezione è stata applicata
            DCSelectionType type = _identifySelectionMethod();
            
            // prima rimuove i vecchi punti di controllo
            DeleteControlPointsManager(entity);

            // aggiungo nuovo control points
            if (select.Value == true)
            {
                // aggiunge alla selezione
                canvas.AddToSelection(entity);

                // notifica selezione
                entity._notifySelectedEvent(type);

                // se è editabile
                if (entity.Editable)
                    AddControlPointsManager(entity);

                // da enfasi
                _addBlink(entity);
            }
            else
            {
                // rimuove enfasi
                _removeBlink(entity);

                // rimuove dalla selezione
                canvas.RemoveFromSelection(entity);

                // forza gli altri flag
                entity.Data[DCDefinitions.KEY_CAPTURED] = false;
                entity.Data[DCDefinitions.KEY_DRAGGING] = false;
                entity.Data[DCDefinitions.KEY_SYS_DRAGGING] = false;

                // notifica deselezione (qua gia è diverso perchè bisogna tenere conto di un flag)
                bool canNotify = (bool)entity.Data[DCDefinitions.FLAG_NOTIFY_DESELECTION];
                if (canNotify)
                    entity._notifyUnSelectedEvent(type);
            }
        }

        // chiamato quando la proprietà position dell'entità è modificata
        internal static void OnPositionChanged(IDCEntity entity, Point newPosition)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // ottiene il canvas (necessario affinchè l'entità possa spostarsi)
            //DCCanvas canvas = entity.GetCanvas();
            //Debug.Assert(canvas != null);
            //if (canvas == null)
            //    throw new ArgumentNullException("canvas");

            FrameworkElement element = entity as FrameworkElement;
            Debug.Assert(element != null);
            if (element == null)
                throw new ArgumentNullException("element");

            // setta la posizione nel canvas
            DCCanvas.SetLeft(element, newPosition.X);
            DCCanvas.SetTop(element, newPosition.Y);

            // aggiorna i punti di controllo sul cambio della posizione
            if (entity.ControlPointsManager != null)
                entity.ControlPointsManager.Update();
        }

        // chiamato quando l'entità viene creata
        internal static void OnCreate(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // setta le proprietà comuni
            entity.Selectable = true;
            entity.Editable = true;
            entity.Deletable = true;
            entity.MoveableType = DCMoveableType.MoveableFree;

            // occorre inizializzare i parametri di left e top altrimenti vale NaN e non è possibile spostare l'oggetto
            entity.Position = new Point(0, 0);

            entity.Data = new Dictionary<string, object>();
            entity.Data[DCDefinitions.KEY_SYS_DRAGGING] = false;
            entity.Data[DCDefinitions.KEY_CAPTURED] = false;
            entity.Data[DCDefinitions.KEY_DRAGGING] = false;
            entity.Data[DCDefinitions.KEY_MOUSE_OVER] = false;
            entity.Data[DCDefinitions.KEY_OLD_MOUSE_POSITION] = null;
            entity.Data[DCDefinitions.KEY_BLINKED] = false;

            entity.Data[DCDefinitions.FLAG_NOTIFY_DESELECTION] = true;

            // campi speciali solo per gli host
            if (entity is DCHost)
            {
                entity.Data[DCDefinitions.KEY_OLD_BACKGROUND] = null;
                entity.Data[DCDefinitions.KEY_OLD_BACKGROUND_2] = null;
                entity.Data[DCDefinitions.KEY_OLD_FOREGROUND] = null;
            }
        }

        // chiamato quando passo sopra all'elemento con il mouse (enter)
        internal static void OnEnter(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // recupero il canvas
            DCCanvas canvas = entity.GetCanvas();

            // e verifico che il canvas ci sia
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // e verifico il tool del canvas
            if (canvas._lastTool != null && !canvas._lastTool.AllowInteraction)
                return;

            // sto creando una selezione? è inutile interpellare l'entità quindi esce
            if (canvas._selection)
                return;

            // set flag
            entity.Data[DCDefinitions.KEY_MOUSE_OVER] = true;

            // non deve sovrascrivere la vecchia entità catturata se è in fase di interazione
            if (canvas._lastCapturedEntity != null)
            {
                if (canvas._lastCapturedEntity.IsCaptured() || canvas._lastCapturedEntity.IsDragging() || canvas._lastCapturedEntity.IsMouseOver())
                    return;
            }

            // altrimenti

            // invalido la grafica dell'entità se è un elemento framework
            FrameworkElement element = entity as FrameworkElement;
            if(element!=null)
                element.InvalidateVisual();

            // setto la nuova entità
            canvas._lastCapturedEntity = entity;
        }

        // chiamato quando passo sopra all'elemento con il mouse (enter)
        internal static void OnLeave(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // recupero il canvas
            DCCanvas canvas = entity.GetCanvas();

            // se il canvas è nullo esce(l'elemento potrebbe essere stato rimosso durante quando mi trovavo ancora nell'onenter)
            if (canvas == null)
                return;

            // e verifico il tool del canvas
            if (canvas._lastTool != null && !canvas._lastTool.AllowInteraction)
                return;

            // invalido la grafica dell'entità
            (entity as FrameworkElement).InvalidateVisual();

            // annullo flag
            entity.Data[DCDefinitions.KEY_MOUSE_OVER] = false;

            // verifico se sto operando sull'entità in oggetto
            if (canvas._lastCapturedEntity == entity)
            {
                // annulla solo se non è sotto interazione
                if (canvas._lastCapturedEntity.IsCaptured() || canvas._lastCapturedEntity.IsDragging() || canvas._lastCapturedEntity.IsMouseOver())
                    return;

                // annullo l'ultima entità catturata
                canvas._lastCapturedEntity = null;
            }
        }
        #endregion


        #region Gestione dei punti di controllo
        // aggiunta/cancellazione punti di controllo
        internal static void DeleteControlPointsManager(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            if (entity.ControlPointsManager != null)
                entity.ControlPointsManager.Clear();
        }

        internal static void AddControlPointsManager(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            if (entity.ControlPointsManager == null)
            {
                if (entity is DCShape)
                    entity.ControlPointsManager = new DCShapeControlPointsMngr(entity);
                else if (entity is DCHost)
                    entity.ControlPointsManager = new DCHostControlPointsMngr(entity);
            }
            else
                entity.ControlPointsManager.Regen();
        }

        // chiamata sul cambio delle proprietà di dipendenza dell'entità
        internal static void RefreshControlPoints(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            IDCEntity entity = dependencyObject as IDCEntity;

            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // ottiene la proprietà Selected
            bool? select = e.NewValue as bool?;

            // recupera canvas
            DCCanvas canvas = entity.GetCanvas();
            // controllo necessario(non vuole l'assert)
            if (canvas == null)
                return;

            if (select == null)
                return;

            // cancella quelli vecchi
            DeleteControlPointsManager(entity);

            // aggiungo nuovamente
            if (select.Value)
            {
                // se è editabile...
                if (entity.Editable)
                    AddControlPointsManager(entity);
            }
        }
        #endregion


        #region Gestione del mouse sul canvas
        internal static void OnMouseDown(DCCanvas canvas, MouseButtonEventArgs e)
        {
            // notifico che sto catturando con il mouse
            canvas._captured = true;

            // notifico vecchia posizione del mouse
            canvas._oldMousePosition = e.GetPosition(canvas);

            // verifico se ho un contesto di TOOL
            if (canvas._lastTool != null)
                canvas._lastTool.OnMouseDown(e);

            // dopo aver eseguito il tool verifica se questo consente l'interazione con la gestione della selezione
            if (canvas._lastTool == null || canvas._lastTool.AllowInteraction)
                DCSelectionMngr.OnMouseDown(canvas, e);

            // solo alla fine verifico se rilasciare il tool
            if (canvas._lastTool != null && canvas._lastTool.IsFinished)
                canvas.SetTool(null);
        }

        internal static void OnMouseUp(DCCanvas canvas, MouseButtonEventArgs e)
        {
            // clear del box della selezione(necessario)
            DCSelectionMngr.ClearSelectionBox(canvas);

            // verifico se ho un contesto di TOOL
            if (canvas._lastTool != null)
            {
                if (!canvas._lastTool.IsAbort && !canvas._lastTool.IsFinished)
                    canvas._lastTool.OnMouseUp(e);
            }

            // dopo aver eseguito il tool verifica se questo consente l'interazione con la gestione della selezione
            if (canvas._lastTool == null || canvas._lastTool.AllowInteraction || canvas._lastTool.IsAbort)
            {
                // processa il tasto destro
                if (e.ChangedButton == MouseButton.Right)
                {
                    // ottiene hit alla posizione del mouse attuale
                    HitTestResult hitTestResult = VisualTreeHelper.HitTest(canvas, e.GetPosition(canvas));

                    // non faccio aprire il context menu quando opero su una textbox
                    if (hitTestResult.VisualHit != null && hitTestResult.VisualHit.GetType().ToString().Contains("System.Windows.Controls.TextBoxView"))
                    {
                        // non fa niente, semplicemente evita che venga aperto il context menu
                    }
                    else
                    {
                        // solo se ho fatto tasto destro su un entità valida
                        if (canvas._lastCapturedEntity != null)
                        {
                            // setto il target di posizione del context menu
                            canvas._contextMenu.PlacementTarget = canvas._lastCapturedEntity as FrameworkElement;

                            // setto l'entità a ricevere il contextmenu
                            (canvas._lastCapturedEntity as FrameworkElement).ContextMenu = canvas._contextMenu;

                            // setto il tag
                            canvas._contextMenu.Tag = canvas._lastCapturedEntity;

                            // apre
                            canvas._contextMenu.IsOpen = true;
                        }
                        else
                        {
                            canvas._contextMenu.IsOpen = false;
                        }
                    }
                }
                else // non ho processato il tasto destro? nasconde il menu'
                {
                    canvas._contextMenu.IsOpen = false;
                }

                DCSelectionMngr.OnMouseUp(canvas, e);
            }

            // solo alla fine verifico se rilasciare il tool
            if (canvas._lastTool != null)
            {
                if (canvas._lastTool.IsFinished || canvas._lastTool.IsAbort)
                    canvas.SetTool(null);
            }

            // notifico rilascio del mouse
            canvas._captured = false;
            
            // notifico rilascio della selezione
            canvas._selection = false;

            // invalida canvas
            canvas.InvalidateVisual();
            canvas.InvalidateMeasure();

            // il canvas deve sempre riottenere il focus per gestire la tastiera a meno che non ci sia gia il focus di tastiera sul figlio(se host)
            if (canvas._lastCapturedEntity != null && canvas._lastCapturedEntity is DCHost)
            {
                DCHost host=canvas._lastCapturedEntity as DCHost;
                if (host.HasInteraction())
                    return;
            } 
            canvas.Focus();
        }

        internal static void OnMouseMove(DCCanvas canvas, MouseEventArgs e)
        {
            // recupero posizione del mouse
            Point mousePosition = e.GetPosition(canvas);

            // notifico coordinate
            if (canvas._coordinatesLbl != null)
                canvas._coordinatesLbl.Content = (int)mousePosition.X + " ; " + (int)mousePosition.Y;

            // verifico se ho un contesto di TOOL
            if (canvas._lastTool != null)
            {
                if (!canvas._lastTool.IsAbort && !canvas._lastTool.IsFinished)
                    canvas._lastTool.OnMouseMove(e);
            }

            // dopo aver eseguito il tool verifica se questo consente l'interazione con la gestione della selezione
            if (canvas._lastTool == null || canvas._lastTool.AllowInteraction || canvas._lastTool.IsAbort)
                DCSelectionMngr.OnMouseMove(canvas, e);

            // solo alla fine verifico se rilasciare il tool
            if (canvas._lastTool != null)
            {
                if (canvas._lastTool.IsFinished || canvas._lastTool.IsAbort)
                    canvas.SetTool(null);
            }

            // aggiorna la posizione del prompt
            DCCanvas.SetLeft(canvas.Prompt, mousePosition.X + 15);
            DCCanvas.SetTop(canvas.Prompt, mousePosition.Y + 15);

            // il canvas deve sempre riottenere il focus per gestire la tastiera a meno che non ci sia gia il focus di tastiera sul figlio(se host)
            if (canvas._lastCapturedEntity != null && canvas._lastCapturedEntity is DCHost)
            {
                DCHost host = canvas._lastCapturedEntity as DCHost;
                if (host.HasInteraction())
                    return;
            }
            canvas.Focus();
        }
        #endregion


        #region Gestione del drag sul canvas
        // si verifica quando si inizia il drag di un oggetto nell'area di disegno
        internal static void OnDragEnter(DCCanvas canvas, DragEventArgs e)
        {
            // verifico se ho un contesto di TOOL
            if (canvas._lastTool != null)
            {
                // termina il tool attivo
                canvas._lastTool.Finish();
            }

            // il tool rilasciato rimuove gli shape di sistema dal disegno incluso il prompt che può dunque essere riaggiunto
            //if (!canvas.Children.Contains(canvas.Prompt))
                canvas.Children.Add(canvas.Prompt);

            DCDragDropMngr.OnDragEnter(canvas, e);
        }

        // si verifica quando il mouse esce fuori dall'area di disegno mentre si dragga un oggetto
        internal static void OnDragLeave(DCCanvas canvas, DragEventArgs e)
        {
            // rimuove il prompt
            canvas.Children.Remove(canvas.Prompt);

            DCDragDropMngr.OnDragLeave(canvas,e);
        }

        // si verifica quando si sta draggando un oggetto nell'area di disegno
        internal static void OnDragOver(DCCanvas canvas, DragEventArgs e)
        {
            // ottiene i dati e il formato dell'oggetto draggato
            object objData=null;
            string format=null;
            _parseDragFormat(e, out objData, out format);

            // ottiene la posizione del mouse
            Point mousePosition = e.GetPosition(canvas);

            // aggiorna la posizione del prompt
            DCCanvas.SetLeft(canvas.Prompt, mousePosition.X + 30);
            DCCanvas.SetTop(canvas.Prompt, mousePosition.Y + 20);

            // esegue le operazioni visive / logiche
            DCDragDropMngr.OnDragOver(canvas, e, objData, format);
        }

        // si verifica quando il drag di un oggetto viene rilasciato all'interno dell'area di disegno
        internal static void OnDrop(DCCanvas canvas, DragEventArgs e)
        {
            // rimuove il prompt
            canvas.Children.Remove(canvas.Prompt);

            // ottiene i dati e il formato dell'oggetto draggato
            object objData = null;
            string format = null;
            _parseDragFormat(e, out objData, out format);

            // esegue le operazioni visive / logiche
            DCDragDropMngr.OnDrop(canvas, e, objData, format);
        }
        #endregion


        #region Gestione della tastiera sul canvas
        internal static void OnKeyDown(DCCanvas canvas, KeyEventArgs e)
        {
            // verifico se ho un contesto di TOOL
            if (canvas._lastTool != null)
            {
                if (!canvas._lastTool.IsAbort && !canvas._lastTool.IsFinished)
                    canvas._lastTool.OnKeyUp(e);
            }

            // dopo aver eseguito il tool verifica se questo consente l'interazione con la gestione della selezione
            if (canvas._lastTool == null || canvas._lastTool.AllowInteraction)
            {
                switch (e.Key)
                {
                    case Key.Up:
                        DCSelectionMngr.PanSelection(canvas, 0, -0.5);
                        break;

                    case Key.Down:
                        DCSelectionMngr.PanSelection(canvas, 0, 0.5);
                        break;

                    case Key.Left:
                        DCSelectionMngr.PanSelection(canvas, -0.5, 0);
                        break;

                    case Key.Right:
                        DCSelectionMngr.PanSelection(canvas, 0.5, 0);
                        break;
                }
            }

            // solo alla fine verifico se rilasciare il tool
            if (canvas._lastTool != null)
            {
                if (canvas._lastTool.IsFinished || canvas._lastTool.IsAbort)
                    canvas.SetTool(null);
            }
        }

        internal static void OnKeyUp(DCCanvas canvas, KeyEventArgs e)
        {
            // verifico se ho un contesto di TOOL
            if (canvas._lastTool != null)
            {
                if (!canvas._lastTool.IsAbort && !canvas._lastTool.IsFinished)
                    canvas._lastTool.OnKeyUp(e);
            }

            // dopo aver eseguito il tool verifica se questo consente l'interazione con la gestione della selezione
            if (canvas._lastTool == null || canvas._lastTool.AllowInteraction)
            {
                switch (e.Key)
                {
                    case Key.Escape:
                        // deseleziona tutto
                        canvas.UnSelectAll();
                        break;

                    case Key.Delete:
                        bool canEraseSelection = true;

                        // recupera posizione del mouse
                        Point mousePos = Mouse.GetPosition(canvas);
                        // recupera elemento toccato dal mouse
                        HitTestResult hitResult=VisualTreeHelper.HitTest(canvas, mousePos);
                        // il mouse ha toccato qualcosa?
                        if (hitResult != null && hitResult.VisualHit != null)
                        {
                            // prova a recuperare il parent dell'oggetto colpito
                            DependencyObject parent = VisualTreeHelper.GetParent(hitResult.VisualHit);

                            // se il parent è un punto di controllo rimuovo il punto di controllo dall'entità toccata(a patto sia uno shape)
                            if (parent != null && parent is DCControlPoint)
                            {
                                // recupero entità associata al punto di controllo
                                DCControlPoint cp = parent as DCControlPoint;
                                if (cp != null)
                                {
                                    // verifica appunto che sia uno shape l'entità gestita
                                    if (cp._entity is DCShape)
                                    {
                                        (cp._entity as DCShape).RemovePoint(cp._index);
                                        canEraseSelection = false;
                                    }
                                }
                            }
                        }

                        // eventualmente si procede con la cancellazione della selezione
                        if(canEraseSelection)
                            canvas.EraseSelection();
                        break;
                }
            }

            // solo alla fine verifico se rilasciare il tool
            if (canvas._lastTool != null)
            {
                if (canvas._lastTool.IsFinished || canvas._lastTool.IsAbort)
                    canvas.SetTool(null);
            }
        }
        #endregion
        
    }
}
