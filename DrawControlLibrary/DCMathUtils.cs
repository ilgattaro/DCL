﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace DrawControlLibrary
{
    /// <summary>
    /// Contiene metodi e algoritmi matematici
    /// </summary>
    public static class DCMathUtils
    {
        public static double GetPerimeter(PointCollection points, DCShapeType shapeType)
        {
            switch (shapeType)
            {
                default:
                    return points.Count;
            }
        }

        public static double GetArea(PointCollection points, DCShapeType shapeType)
        {
            switch (shapeType)
            {
                default:
                    return points.Count*2;
            }
        }

        public static double GetDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }
    }
}
