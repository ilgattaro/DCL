﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.ComponentModel;
using DrawControlLibrary;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using System.Windows.Interop;
using DClExtensions;
using DrawControlLibrary.Entities;
using System.Threading.Tasks;
using System.Threading;
using CommonClassesLibrary;

namespace KDraw
{
    public partial class MainWindow : Window
    {
        public MainWindow() {
            InitializeComponent();

#if ALPHA_VER
            MessageBox.Show("Questa è una versione ALPHA.\n\nAlcune funzionalità potrebbero non funzionare correttamente o essere disponibili.","KDraw");
#endif
        }

        private void root_Loaded(object sender, RoutedEventArgs e)
        {
            CommonUtilities.RunAsync((data) =>
            {
                editor.Dispatcher.BeginInvoke(new Action(() => {
                    editor.AddDocument("Test doc 1");
                    editor.AddDocument("Document 2");
                    editor.AddDocument("New 3");
                }));
            },500);

            editor.DirectoryOfImages = Directory.GetCurrentDirectory() + @"\cliparts";

            editor.EnableMenu(true);
        }
    }
}
