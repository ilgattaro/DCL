﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TotalDraw
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void InkCanvas_EditingModeChanged(object sender, RoutedEventArgs e)
        {

        }

        private void InkCanvas_Initialized(object sender, EventArgs e)
        {

        }

        private void InkCanvas_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void InkCanvas_SelectionMoved(object sender, EventArgs e)
        {

        }

        private void InkCanvas_SelectionResized(object sender, EventArgs e)
        {

        }

        private void InkCanvas_StrokeErased(object sender, RoutedEventArgs e)
        {

        }
    }
}
