﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Windows;

namespace CommonClassesLibrary
{
    /// <summary>
    /// Utilità varie
    /// </summary>
    public static class CommonUtilities
    {
        /// <summary>
        /// Ottiene il nome del metodo chiamante
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static string GetCallingMethod(int level)
        {
            StackTrace stackTrace = new StackTrace();           // get call stack
            StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

            int i = 0;
            foreach (StackFrame stackFrame in stackFrames)
            {
                if (i == level)
                    return stackFrame.GetMethod().Name;
                i++;
            }
            return null;
        }

        /// <summary>
        /// Esegue un azione async, chiamare Dispatcher.BeginInvoke per le modifiche sull'interfaccia
        /// </summary>
        /// <param name="action"></param>
        /// <param name="delay"></param>
        /// <param name="data"></param>
        public static void RunAsync(Action<object> action, int delay, object data=null)
        {
            Thread t = new Thread(new ParameterizedThreadStart((obj) =>
            {
                Thread.Sleep(delay);
                action(obj);
                //Application.Current.Dispatcher.BeginInvoke(new Action(() => { action(obj); }));
            }));
            t.SetApartmentState(ApartmentState.STA);
            t.Start(data);
        }

        public static void Wait(int s)
        {
            DateTime next = DateTime.Now.AddSeconds(s);
            double sec=0;
            do
            {
                sec = next.Subtract(DateTime.Now).TotalSeconds;
            } while (sec > 0);
        }
    }
}
